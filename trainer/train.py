import numpy as np 
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.svm import LinearSVC
from sklearn.ensemble import RandomForestClassifier
import pickle
from joblib import dump, load
training = pd.read_csv("trainer/train.csv")
#print(training.head()) # print first 5 rows
training = training.drop(['Name','Ticket', 'Cabin'], axis=1)
# Filling missing age values with median
training.Age = training.Age.fillna(training.Age.median())
training.Embarked = training.Embarked.fillna('S')
print(training.info())
embark_dummies_titanic  = pd.get_dummies(training['Embarked'])
sex_dummies_titanic  = pd.get_dummies(training['Sex'])
pclass_dummies_titanic  = pd.get_dummies(training['Pclass'], prefix="Class")
training = training.drop(['Embarked', 'Sex', 'Pclass'], axis=1)
titanic = training.join([embark_dummies_titanic, sex_dummies_titanic, pclass_dummies_titanic])
print(titanic.head())


X_all = titanic.drop('Survived', axis=1)
y_all = titanic.Survived
X_all.to_csv('trainer/test.csv')
X_all.set_index('PassengerId', inplace=True)
num_test = 0.20
X_train, X_test, y_train, y_test = train_test_split(X_all, y_all, test_size=num_test, random_state=23)

rf_clf = RandomForestClassifier()
rf_clf.fit(X_train, y_train)
pred_rf = rf_clf.predict(X_test)
acc_rf = accuracy_score(y_test, pred_rf)
print(acc_rf)

dump(rf_clf, 'rf_clf.joblib')
